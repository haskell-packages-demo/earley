# Earley

Parsing all context-free grammars using Earley's algorithm. https://www.stackage.org/package/Earley

## Unofficial documentation
* [*Earley parser*](https://en.wikipedia.org/wiki/Earley_parser) (Wikipedia)